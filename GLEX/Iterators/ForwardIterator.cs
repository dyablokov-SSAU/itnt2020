﻿using System;

namespace Glex.Iterators
{
    public abstract  class ForwardIterator<I> //: TraversalIterator<I>
    {
        public Func<I, bool> EqualTo { get; }
        public Func<I, bool> CompatibleWith { get; }
    };
}
