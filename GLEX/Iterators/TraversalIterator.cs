﻿namespace Glex.Iterators
{
    public ref struct TraversalIterator<IT>
    {
        private readonly IT _iteratorTraits;

        public TraversalIterator<IT> Advance()
        {
            return this;
        }

        public void Dispose()
        {
        }

        public TraversalIterator(TraversalIterator<IT> other)
        {
            _iteratorTraits = other._iteratorTraits;
        }
    };
}