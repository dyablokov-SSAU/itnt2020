﻿using System;

namespace Glex.Iterators
{
    public abstract class TraversalIteratorTraits<I>
    {
        public Action Advance { get; }
        public Action Dispose { get; }

        //public TraversalIteratorTraits<I> New()
    };
}
