﻿namespace Glex.Enumerable
{
    public interface IBidirectionalEnumerator<E, T> : IForwardEnumerator<E, T>
    {
        bool MovePrior();
    };
}
