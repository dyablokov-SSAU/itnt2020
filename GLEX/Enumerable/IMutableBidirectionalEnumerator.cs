﻿namespace Glex.Enumerable
{
    public interface IMutableBidirectionalEnumerator<E, T> : IBidirectionalEnumerator<E, T>, IMutableForwardEnumerator<E, T>
    {
    };
}
