﻿namespace Glex.Enumerable
{
    public interface IMutableRandomAccessEnumerator<E, D, T> : IRandomAccessEnumerator<E, D, T>, IMutableBidirectionalEnumerator<E, T>
    {
        new T this[D pos] { get; set; }

        T IRandomAccessEnumerator<E, D, T>.this[D pos] => this[pos];
    };
}
