﻿namespace Glex.Enumerable
{
    public interface IRandomAccessEnumerator<E, D, T> : IBidirectionalEnumerator<E, T>
    {
        bool MoveAt(D pos);

        T this[D pos] { get; }
        
        D Length { get; }
        D Position { get; }
    };
}
