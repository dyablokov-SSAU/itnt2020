﻿using System.Collections;
using System.Collections.Generic;

namespace Glex.Enumerable
{
    public interface IMutableForwardEnumerator<E, T> : IForwardEnumerator<E, T>, IOutputEnumerator<E, T>
    {
        new T Current {get; set; }

        object IEnumerator.Current => Current;

        T IEnumerator<T>.Current => Current;

        T IOutputEnumerator<E, T>.Current { set => Current = value;}
    };
}