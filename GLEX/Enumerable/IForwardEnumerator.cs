﻿using System.Collections;
using System.Collections.Generic;

namespace Glex.Enumerable
{
    public interface IForwardEnumerator<E, T> : IEnumerableEnumerator<E, T>
    {
        object IEnumerator.Current => Current;
    };
}