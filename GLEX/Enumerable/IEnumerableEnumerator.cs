﻿using System.Collections;
using System.Collections.Generic;

namespace Glex.Enumerable
{
    public interface IEnumerableEnumerator<E, T> : IEnumerator<T>
    {
        E GetEnumerable();

        object IEnumerator.Current => Current;
    };
}
