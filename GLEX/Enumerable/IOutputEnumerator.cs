﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Glex.Enumerable
{
    public interface IOutputEnumerator<E, T> : IEnumerableEnumerator<E, T>
    {
        new T Current { set; }
        
        object IEnumerator.Current => throw new InvalidOperationException();

        T IEnumerator<T>.Current => throw new InvalidOperationException();
    };
}
